package gitleaks

import (
	"log"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

func TestToIssues(t *testing.T) {
	in := `[
		{
			"line": "-----BEGIN RSA PRIVATE KEY-----",
			"commit": "5ec1e27c70c81b5cc060eed22166790bd59286f1",
			"offender": "-----BEGIN RSA PRIVATE KEY-----",
			"reason": "RSA",
			"commitMsg": "i ",
			"author": "Gilbert Roulot \u003cgroulot@gitlab.com\u003e",
			"file": "testdata/id_rsa",
			"repo": ".",
			"date": "2019-02-15T14:38:12+01:00"
		},
		{
			"line": "const AWS_ID = \"AKIAIOSFODNN7EXAMPLE\"",
			"commit": "5ec1e27c70c81b5cc060eed22166790bd59286f1",
			"offender": "AKIAIOSFODNN7EXAMPLE",
			"reason": "AWS",
			"commitMsg": "i ",
			"author": "Gilbert Roulot \u003cgroulot@gitlab.com\u003e",
			"file": "testdata/main.go",
			"repo": ".",
			"date": "2019-02-15T14:38:12+01:00"
		},
		{
			"line": "const WORDPRESS = \"XahV8uKN^/MvXC` + "`" + `4U+ZI!Q$0XvomTTHAM-[Wu|hg1B[P+%-8$y-%W35qW2hW9 HZ\"",
			"commit": "5ec1e27c70c81b5cc060eed22166790bd59286f1",
			"offender": "\"XahV8uKN^/MvXC` + "`" + `4U+ZI!Q$0XvomTTHAM-[Wu|hg1B[P+%-8$y-%W35qW2hW9",
			"reason": "Entropy: 5.30",
			"commitMsg": "i ",
			"author": "Gilbert Roulot \u003cgroulot@gitlab.com\u003e",
			"file": "testdata/main.go",
			"repo": ".",
			"date": "2019-02-15T14:38:12+01:00"
		},
		{
			"line": "const SSHKEY = ` + "`" + `-----BEGIN RSA PRIVATE KEY-----",
			"commit": "5ec1e27c70c81b5cc060eed22166790bd59286f1",
			"offender": "-----BEGIN RSA PRIVATE KEY-----",
			"reason": "RSA",
			"commitMsg": "i ",
			"author": "Gilbert Roulot \u003cgroulot@gitlab.com\u003e",
			"file": "testdata/main.go",
			"repo": ".",
			"date": "2019-02-15T14:38:12+01:00"
		},
		{
			"line": "const STRIPE = \"sk_test_4eC39HqLyjWDarjtT1zdp7dc\"",
			"commit": "6b997965bc2146f957c9d736eb4918ac644847b8",
			"offender": "sk_test_4eC39HqLyjWDarjtT1zdp7dc",
			"reason": "Stripe",
			"commitMsg": "Add test api keys ",
			"author": "Philippe Lafoucrière \u003cplafoucriere@gitlab.com\u003e",
			"file": "testdata/main.go",
			"repo": ".",
			"date": "2019-03-23T12:51:19-04:00"
		},
		{
			"line": "const API_KEY = \"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"",
			"commit": "6b997965bc2146f957c9d736eb4918ac644847b8",
			"offender": "API_KEY = \"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"",
			"reason": "Generic API Key",
			"commitMsg": "Add test api keys ",
			"author": "Philippe Lafoucrière \u003cplafoucriere@gitlab.com\u003e",
			"file": "testdata/main.go",
			"repo": ".",
			"date": "2019-03-23T12:51:19-04:00"
		},
		{
			"line": "const APIKEY = \"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"",
			"commit": "6b997965bc2146f957c9d736eb4918ac644847b8",
			"offender": "APIKEY = \"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"",
			"reason": "Generic API Key",
			"commitMsg": "Add test api keys ",
			"author": "Philippe Lafoucrière \u003cplafoucriere@gitlab.com\u003e",
			"file": "testdata/main.go",
			"repo": ".",
			"date": "2019-03-23T12:51:19-04:00"
		},
		{
			"line": "const APITOKEN = \"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"",
			"commit": "e4f26145aa93c1f1e45a39d4c1bbc2a097d14565",
			"offender": "APITOKEN = \"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"",
			"reason": "Generic API Key",
			"commitMsg": "Replace broken php file by a go one ",
			"author": "Philippe Lafoucrière \u003cplafoucriere@gitlab.com\u003e",
			"file": "testdata/main.go",
			"repo": ".",
			"date": "2019-03-23T17:18:16-04:00"
		},
		{
			"line": "const API_TOKEN = \"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"",
			"commit": "e4f26145aa93c1f1e45a39d4c1bbc2a097d14565",
			"offender": "API_TOKEN = \"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"",
			"reason": "Generic API Key",
			"commitMsg": "Replace broken php file by a go one ",
			"author": "Philippe Lafoucrière \u003cplafoucriere@gitlab.com\u003e",
			"file": "testdata/main.go",
			"repo": ".",
			"date": "2019-03-23T17:18:16-04:00"
		}
	]`
	want := []issue.Issue{
		{
			Category:    issue.CategorySast,
			Name:        "RSA private key",
			Message:     "RSA private key",
			Description: "RSA private key detected; please remove and revoke it if this is a leak.",
			CompareKey:  "testdata/id_rsa:8bcac7908eb950419537b91e19adc83ce2c9cbfdacf4f81157fdadfec11f7017:RSA",
			Severity:    issue.LevelCritical,
			Confidence:  issue.LevelUnknown,
			Scanner: issue.Scanner{
				ID:   "gitleaks",
				Name: "Gitleaks",
			},
			Location: issue.Location{
				File:      "testdata/id_rsa",
				LineStart: 1,
				LineEnd:   15,
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID RSA",
					Value: "RSA",
				},
			},
		},
		{
			Category:    issue.CategorySast,
			Name:        "AWS API key",
			Message:     "AWS API key",
			Description: "Amazon Web Services API key detected; please remove and revoke it if this is a leak.",
			CompareKey:  "testdata/main.go:8a22accb113d641b78b389ccce92fca96acbe2fd4f1701ead6783768fdbe9d8a:AWS",
			Severity:    issue.LevelCritical,
			Confidence:  issue.LevelUnknown,
			Scanner: issue.Scanner{
				ID:   "gitleaks",
				Name: "Gitleaks",
			},
			Location: issue.Location{
				File:      "testdata/main.go",
				LineStart: 7,
				LineEnd:   7,
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID AWS",
					Value: "AWS",
				},
			},
		},
		{
			Category:    issue.CategorySast,
			Name:        "High entropy string",
			Message:     "High entropy string",
			Description: "A string with high entropy was found, this could be a secret",
			CompareKey:  "testdata/main.go:4bc941a5c41c5460ab3d1895453e66d7032ae11dd401154d396f906d4dda0add:Entropy",
			Severity:    issue.LevelCritical,
			Confidence:  issue.LevelUnknown,
			Scanner: issue.Scanner{
				ID:   "gitleaks",
				Name: "Gitleaks",
			},
			Location: issue.Location{
				File:      "testdata/main.go",
				LineStart: 9,
				LineEnd:   9,
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID Entropy",
					Value: "Entropy",
				},
			},
		},
		{
			Category:    issue.CategorySast,
			Name:        "RSA private key",
			Message:     "RSA private key",
			Description: "RSA private key detected; please remove and revoke it if this is a leak.",
			CompareKey:  "testdata/main.go:430e004686f8f9c2d11ce84da58bd94d1fceb70b0296e46dd8e1ca059ebf7e92:RSA",
			Severity:    issue.LevelCritical,
			Confidence:  issue.LevelUnknown,
			Scanner: issue.Scanner{
				ID:   "gitleaks",
				Name: "Gitleaks",
			},
			Location: issue.Location{
				File:      "testdata/main.go",
				LineStart: 11,
				LineEnd:   40,
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID RSA",
					Value: "RSA",
				},
			},
		},
		{
			Category:    "sast",
			Name:        "Stripe",
			Message:     "Stripe",
			Description: "Stripe API key detected; please remove and revoke it if this is a leak.",
			CompareKey:  "testdata/main.go:0b16c77410a5918254c1b2bdc7576b87d9ffc7dcedf29b7bdca0b423f6209009:Stripe",
			Severity:    7,
			Confidence:  2,
			Solution:    "",
			Scanner: issue.Scanner{
				ID:   "gitleaks",
				Name: "Gitleaks",
			},
			Location: issue.Location{
				File:      "testdata/main.go",
				LineStart: 50,
				LineEnd:   50,
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID Stripe",
					Value: "Stripe",
				}},
		},
		{
			Category:    "sast",
			Name:        "Generic API Key",
			Message:     "Generic API Key",
			Description: "Unknown API key detected; please remove and revoke it if this is a leak.",
			CompareKey:  "testdata/main.go:9ac057c201cda016677f2ddeb03d4c59991007a13b9e07917d1f8782e9564970:Generic API Key",
			Severity:    7,
			Confidence:  2,
			Solution:    "",
			Scanner: issue.Scanner{ID: "gitleaks",
				Name: "Gitleaks"},
			Location: issue.Location{File: "testdata/main.go",
				LineStart: 52,
				LineEnd:   52,
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID Generic API Key",
					Value: "Generic API Key",
				}},
			Links: []issue.Link(nil)},
		{
			Category:    "sast",
			Name:        "Generic API Key",
			Message:     "Generic API Key",
			Description: "Unknown API key detected; please remove and revoke it if this is a leak.",
			CompareKey:  "testdata/main.go:f4ac6d9fd61b258a20a5d9c5aea0e0b48abc60d50edd149021d875066e6c592b:Generic API Key",
			Severity:    7,
			Confidence:  2,
			Solution:    "",
			Scanner: issue.Scanner{ID: "gitleaks",
				Name: "Gitleaks"},
			Location: issue.Location{File: "testdata/main.go",
				LineStart: 53,
				LineEnd:   53,
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID Generic API Key",
					Value: "Generic API Key",
				}},
		},
		{
			Category:    "sast",
			Name:        "Generic API Key",
			Message:     "Generic API Key",
			Description: "Unknown API key detected; please remove and revoke it if this is a leak.",
			CompareKey:  "testdata/main.go:72c5f23ed973b0cc312a2c05cae2798ec924f82e3154c86af53e9db0101ec22c:Generic API Key",
			Severity:    7,
			Confidence:  2,
			Solution:    "",
			Scanner: issue.Scanner{ID: "gitleaks",
				Name: "Gitleaks"},
			Location: issue.Location{File: "testdata/main.go",
				LineStart: 54,
				LineEnd:   54,
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID Generic API Key",
					Value: "Generic API Key",
				}},
		},
		{
			Category:    "sast",
			Name:        "Generic API Key",
			Message:     "Generic API Key",
			Description: "Unknown API key detected; please remove and revoke it if this is a leak.",
			CompareKey:  "testdata/main.go:ecd55deb344afa69c181ae430c0352de88dd565ab7ed454535824d62f18882fd:Generic API Key",
			Severity:    7,
			Confidence:  2,
			Solution:    "",
			Scanner: issue.Scanner{ID: "gitleaks",
				Name: "Gitleaks"},
			Location: issue.Location{File: "testdata/main.go",
				LineStart: 55,
				LineEnd:   55,
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID Generic API Key",
					Value: "Generic API Key",
				}},
		},
	}
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	r := strings.NewReader(in)
	got, err := toIssues(os.Stderr, filepath.Join(dir, "..", "..", "test", "fixtures"), r)
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
